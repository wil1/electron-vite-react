FROM gitpod/workspace-full-vnc

RUN sudo apt-get update \
    && sudo apt-get install -y libgbm-dev libnss3-dev \
    && sudo apt-get clean

USER gitpod
